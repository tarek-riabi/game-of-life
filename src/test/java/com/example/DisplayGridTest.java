package com.example;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DisplayGridTest {

    DisplayGrid displayer;
    Grid grid;

    @BeforeEach
    public void init() {
        grid = new Grid();
        displayer = new DisplayGrid(grid);
    }

    @Test
    public void displayer_of_empty_grid_should_display_nothing() {
        assertEquals("", displayer.display());
    }

    @Test
    public void displayer_of_one_cell_grid_should_display_one_star() {
        grid.addCell(0,0);
        assertEquals("*", displayer.display());
    }

    @Test
    public void displayer_of_two_cell_grid_should_display_two_stars() {
        grid.addCell(0,0);
        grid.addCell(1,0);
        assertEquals("**", displayer.display());
    }

    @Test
    public void displayer_of_two_separated_cell_grid_should_display_two_separated_stars() {
        grid.addCell(0,0);
        grid.addCell(2,0);
        assertEquals("*-*", displayer.display());
    }

    @Test
    public void displayer_of_two_vertically_separated_cell_grid_should_display_two_separated_stars() {
        grid.addCell(0,0);
        grid.addCell(0,1);
        assertEquals("*\n*", displayer.display());
    }

    @Test
    public void displayer_of_two_vertically_separated_not_neighbours_cell_grid_should_display_two_separated_stars() {
        grid.addCell(0,0);
        grid.addCell(0,2);
        assertEquals("*\n-\n*", displayer.display());
    }
}
