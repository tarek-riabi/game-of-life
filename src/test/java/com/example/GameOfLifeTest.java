package com.example;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/*
Any live cell with fewer than two live neighbours dies,
as if by underpopulation.
Any live cell with two or three live neighbours lives on
to the next generation.
Any live cell with more than three live neighbours dies,
as if by overpopulation.
Any dead cell with exactly three live neighbours
becomes a live cell, as if by reproduction.
 */
public class GameOfLifeTest {

    private Grid grid;

    @BeforeEach
    public void init() {
        grid = new Grid();
    }

    @Test
    public void empty_grid_should_yield_empty_grid() {
        assertTrue(grid.isEmpty());
        grid.next();
        assertTrue(grid.isEmpty());
    }

    @Test
    public void unique_cell_should_die() {
        grid.addCell(0,0);
        assertFalse(grid.isEmpty());
        grid.next();
        assertTrue(grid.isEmpty());
    }

    @Test
    public void two_adjacent_cells_should_die() {
        grid.addCell(0,0);
        grid.addCell(1,0);
        assertFalse(grid.isEmpty());
        grid.next();
        assertTrue(grid.isEmpty());
    }

    @Test
    public void three_adjacent_cells_middle_cell_should_live() {
        grid.addCell(0,0);
        grid.addCell(1,0);
        grid.addCell(2,0);
        assertFalse(grid.isEmpty());
        grid.next();
        Cell cell = new Cell(1, 0);
        assertTrue(grid.hasLiveCell(cell));
    }

    @Test
    public void three_adjacent_cells_leftmost_cell_should_die() {
        grid.addCell(0,0);
        grid.addCell(1,0);
        grid.addCell(2,0);
        assertFalse(grid.isEmpty());
        grid.next();
        assertTrue(grid.hasLiveCell(new Cell(1, 0)));
        assertFalse(grid.hasLiveCell(new Cell(0, 0)));
    }

    @Test
    public void three_adjacent_cells_rightmost_cell_should_die() {
        grid.addCell(0,0);
        grid.addCell(1,0);
        grid.addCell(2,0);
        assertFalse(grid.isEmpty());
        grid.next();
        assertTrue(grid.hasLiveCell(new Cell(1, 0)));
        assertFalse(grid.hasLiveCell(new Cell(2, 0)));
    }

    @Test
    public void five_adjacent_cells_middle_cell_should_die() {
        grid.addCell(0,0);
        grid.addCell(-1,0);
        grid.addCell(0,1);
        grid.addCell(1,0);
        grid.addCell(0,-1);
        assertFalse(grid.isEmpty());
        grid.next();
        assertFalse(grid.hasLiveCell(new Cell(0, 0)));
    }

    @Test
    public void three_adjacent_cells_middle_dead_cell_should_become_live() {
        grid.addCell(0,0);
        grid.addCell(0,1);
        grid.addCell(1,0);
        grid.next();
        assertTrue(grid.hasLiveCell(new Cell(1, 1)));
    }

    @Test
    public void test_complex_example_1() {
        grid.addCell(0,0);
        grid.addCell(1,1);
        grid.addCell(2,2);
        grid.addCell(1,-1);
        grid.addCell(2,-2);
        grid.next();
        assertTrue(grid.hasLiveCell(new Cell(0, 0)));
        assertTrue(grid.hasLiveCell(new Cell(1, 0)));
        assertTrue(grid.hasLiveCell(new Cell(1, 1)));
        assertTrue(grid.hasLiveCell(new Cell(1, -1)));
        assertEquals(0, grid.getMinX());
        assertEquals(1, grid.getMaxX());
        assertEquals(-1, grid.getMinY());
        assertEquals(1, grid.getMaxY());
    }
}
