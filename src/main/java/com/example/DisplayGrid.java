package com.example;

public class DisplayGrid {

    private final Grid grid;

    public DisplayGrid(Grid grid) {
        this.grid = grid;
    }

    public String display() {
        StringBuilder result = new StringBuilder();
        if (this.grid.isEmpty())
            return result.toString();

        int maxY = this.grid.getMaxY() ;
        int minY = this.grid.getMinY();
        int minX = this.grid.getMinX();
        int maxX = this.grid.getMaxX();

        for (int y = maxY; y >= minY; y--) {
            StringBuilder line = new StringBuilder();
            for (int x = minX; x <= maxX; x++) {
                Cell cell = new Cell(x, y);
                if (this.grid.hasLiveCell(cell)) {
                    line.append("*");
                } else {
                    line.append("-");
                }
            }
            result.append(line).append("\n");
        }
        return result.toString().trim();
    }
}
