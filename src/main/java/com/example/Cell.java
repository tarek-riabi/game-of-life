package com.example;

import java.util.HashSet;
import java.util.Set;

public record Cell(Integer x, Integer y) {

    public static final int MIN_NEIGHBOURS_TO_SURVIVE = 2;
    public static final int MAX_NEIGHBOURS_TO_SURVIVE = 3;
    public static final int REQUIRED_NEIGHBOURS_TO_BECOME_LIVE = 3;

    public Cell {
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }
    }

    public boolean shouldSurvive(Set<Cell> cells) {
        int neighbours = countLiveNeighbours(cells);
        return neighbours >= MIN_NEIGHBOURS_TO_SURVIVE
                && neighbours <= MAX_NEIGHBOURS_TO_SURVIVE;
    }

    public boolean shouldResurrect(Set<Cell> cells) {
        return countLiveNeighbours(cells) == REQUIRED_NEIGHBOURS_TO_BECOME_LIVE;
    }

    public Set<Cell> findAllNeighbours() {
        Set<Cell> allNeighbours = new HashSet<>();
        for (int i = this.x() - 1; i <= this.x() + 1; i++) {
            for (int j = this.y() - 1; j <= this.y() + 1; j++) {
                if (this.x() != i || this.y() != j) {
                    allNeighbours.add(new Cell(i, j));
                }
            }
        }
        return allNeighbours;
    }

    private int countLiveNeighbours(Set<Cell> otherCells) {
        return otherCells.stream().filter(this::isLiveNeighbour).toList().size();
    }

    private boolean isLiveNeighbour(Cell otherCell) {
        return !this.equals(otherCell)
                && Math.abs(this.x() - otherCell.x()) <= 1
                && Math.abs(this.y() - otherCell.y()) <= 1;
    }

    boolean isLowerThan(Cell other) {
        return this.y() < other.y();
    }

    boolean isToTheLeftOf(Cell other) {
        return this.x() < other.x();
    }

    boolean isOnSameLineAs(Cell other) {
        return this.y().equals(other.y());
    }

    boolean isOnSameColumnAs(Cell other) {
        return this.x().equals(other.x());
    }
}
