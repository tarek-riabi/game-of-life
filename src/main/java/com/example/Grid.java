package com.example;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Grid {

    private final Set<Cell> cells = new HashSet<>();

    public void next() {
        if (this.isEmpty()) {
            return;
        }
        List<Cell> liveCellsToSurvive = cells.stream().filter(cell -> cell.shouldSurvive(cells)).toList();
        List<Cell> deadCellsToBecomeLive = this.cells.stream().flatMap(
                cell -> cell.findAllNeighbours().stream().filter(
                        neighbour -> neighbour.shouldResurrect(this.cells))
        ).toList();
        this.cells.clear();
        this.cells.addAll(liveCellsToSurvive);
        this.cells.addAll(deadCellsToBecomeLive);
    }

    int getMinX() {
        return getXAxisExtremum(true);
    }

    int getMaxX() {
        return getXAxisExtremum(false);
    }

    int getXAxisExtremum(boolean first) {
        List<Cell> list = new ArrayList<>(cells);
        list.sort((cell1, cell2) -> {
            if (cell1.isToTheLeftOf(cell2)) return -1;
            if (cell1.isOnSameColumnAs(cell2)) return 0;
            return 1;
        });
        return first ? list.get(0).x() : list.get(cells.size() - 1).x();
    }

    int getYAxisExtremum(boolean first) {
        List<Cell> list = new ArrayList<>(cells);
        list.sort((cell1, cell2) -> {
            if (cell1.isLowerThan(cell2)) return -1;
            if (cell1.isOnSameLineAs(cell2)) return 0;
            return 1;
        });
        return first ? list.get(0).y() : list.get(cells.size() - 1).y();
    }

    int getMinY() {
        return getYAxisExtremum(true);
    }

    int getMaxY() {
        return getYAxisExtremum(false);
    }

    public boolean isEmpty() {
        return cells.isEmpty();
    }

    public void addCell(int x, int y) {
        cells.add(new Cell(x, y));
    }

    public Boolean hasLiveCell(Cell cell) {
        return cells.stream().anyMatch(tmp -> tmp.equals(cell));
    }


}
